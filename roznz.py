#!/usr/bin/env python
#
#   2020 0211
#          read nz tab just like a boss
#
#       use Requestd and all that shit:
# https://www.soudegesu.com/en/post/python/http-request-with-urllib/
#
#
# stack overflow 27835619
#       turn off verification. (for tab.ubet.com)
#
# 
import json
import os
import ssl
import sqlite3
from  urllib.request import *

ssl._create_default_https_context = ssl._create_unverified_context
#
#  dictionary which defines sql columns        
#
real = 'real'
num = integer  = 'int'
varchar = char = 'varchar'
OZ_ME_COLUMNS = { "nz_pk": " integer constraint nz_pk PRIMARY KEY autoincrement",         \
      "tdom":varchar,"nz_meet_no":num,"nz_venue":varchar,"nz_race":num,"nz_hno":num,    \
      "nz_hna":varchar,"nz_twt":real,"nz_tbar":num,"nz_jna":varchar,"nz_fpo":real       \
            }
OZ_ME_CONSTRAINT = "nz_hna,tdom,nz_venue,nz_race"
#
#        change local 
#
LOCAL = "/usr/local/share/public"
db_dir = "%s/db" % LOCAL
db_db  = "roznz.db"

def lib_use_db( sqdir, sqfile ):
  '''
           sqlite3 open db, and trun off blocking writes
  '''
  DB = "%s/%s" %( sqdir, sqfile.lower() )
  try:
    fi = open( DB )
    fi.close()
    con = sqlite3.connect( DB )
    curs = con.cursor()
    curs.execute( "PRAGMA synchronous = off" )
    return curs,con
  except:
    print( "\n%s * * * * * * * * database does not exist * * * *\n" %( DB ))
  exit(-4)
#
def lib_create_db( sqdir,sqfile ):
  '''
         create sqlite3 database file
             and return  use_db
  '''
  DB = "%s/%s" %( sqdir, sqfile.lower() )
  if os.path.isfile( DB ):
    pass
  else:
    sqfp = open( DB , "w" )
    if sqfp:
      sqfp.close()
    else:
      print( "error cant create databse ::%s" %( DB ))
  return lib_use_db( sqdir,sqfile )
#
def lib_sq_read( curs,sql ):
  '''
        read sql
   dictionary retains order in python these days
    if this does not happen, update your python
  '''
  pass

def lib_sq_write( dic,curs,con,table ):
  '''
          write sql into table

        insert into table blaa(columns) values(vals) on conflict do nothing

       dictionary contains column names and data
         checks for conflict, and does nothing
  '''
  sqin = "insert into %s( " %( table )
  sqer = " on conflict do nothing"
  sqva = "values( "
  for dakey in dic:
    sqin = "%s%s," %( sqin, dakey )
    sqva = "%s\'%s\'," %( sqva, dic[dakey] )
  sqin = "%s )" %( sqin )
  sqin = sqin.replace( ', )'," )" )
  sqva = "%s )" %( sqva )
  sqva = sqva.replace( ', )', " )" )
  SQEX = "%s%s%s" %( sqin, sqva, sqer )
#  print( SQEX )
  curs.execute( SQEX )
  con.commit()
#
#
def sq_modat( curs ):
  '''

  '''
  pass
#
def lib_sq_table( curs, table, columns, constraint ):
  '''
          create sql command to create a table
          create dict to reflect that table
  '''
  ta_cols = ""
  sqid = {}
  for doco in columns.keys():
    ta_cols = "%s%s %s, " %( ta_cols, doco, columns[ doco ] )
#
#      do not place primar y key in data dict
#
    if "autoincrement" in columns[ doco ].lower():     continue
    if "primary key"   in columns[ doco ].lower():     continue
    sqid[ doco ] = None
  if constraint != None:
    if len( constraint ) > 2:
      SQC = 'CREATE TABLE IF NOT EXISTS %s( %s UNIQUE( %s ))' %( table, ta_cols, constraint )
      return SQC, sqid
  SQC = 'CREATE TABLE IF NOT EXISTS %s( %s )' %( table, ta_cols )
  return SQC.replace( ',  )', " )" ), sqid
#
#
#
def ubug():
  '''
         this does nothing... because
     it was debugging

  '''
  pass
  '''
    print(type(mei))
    print("MEI keys::::",mei.keys())
#
    for keik in mei.keys():
      print( keik,type(mei[keik]))
#
    print("\n\n")
    print("betslip_type",mei["betslip_type"] )
    print("code", mei["code"] )
    print("country", mei["country"] )
    print("name", mei["name"] )
    print("number", mei["number"] )# ########### meeting code Mnn
    print("nz", mei["nz"] )
    print("penetrometer", mei["penetrometer"] )
    print("races",type( mei["races"] ))
    print("status", mei["status"] )
    print("track_dir", mei["track_dir"] )
    print("type", mei["type"] )
    print("venue", mei["venue"] )

    print("\n\n")
#    print( oddsin["meetings"][ ea_oz_me ].keys())
    oei = oddsin["meetings"][ ea_oz_me ]
    for oeik in oei:
      print( oeik,type(oei[oeik]))
    print( "O D D S number",oei["number"])
    exit()
  exit()

  while ski:
  '''
####################################################
#
def read_mee( url ):
  '''

      read NZ tab json site

           #tp://stackoverflow.com/questions/9839179/https-request-in-py
url = "https://www.tab.co.nz/racing/"
url = "https://www.tab.co.nz/odds/"
            #tp://stackoverflow.com/questions/9839179/https-request-in-py

  '''
#
#         U_A fakes a browser    nz tab requires a browser.
#
  U_A = 'Mozilla/5.0 ( Windows: U; Windows NT 5.1; it; rv; 1.8.1.11) Gecko/20071127 Firefox/2.0.0.11'
#
#
  REQ = Request( url=url, headers={ 'User-Agent': U_A }, method='GET' )
#
  with urlopen(REQ) as xex:
    bla = xex.read().decode()
  return json.loads( bla )
#
#------------------------------------------------------------------
#
def only_aus( all_meets ):
  '''
     discover OZ meetings in schedule
       only keep horses

        input list of meeting
        output index list of aus races
          no dogs no harness

  '''
  aus_mee = []
  for me_ in range(len(all_meets)):
    me_da = all_meets[me_]
    if me_da["country"] != "aus":    continue
    if "aces" not in me_da["name"]:  continue
    if "Dogs"     in me_da["name"]:  continue
    if "Harness"  in me_da["name"]:  continue
    aus_mee.append(me_)
  return aus_mee
#
#------------------------------------------------------------------
#
def OZ_horses( sren,oren,race_dict,curs,con,table ):
  '''
          join horse and fixed odds
    save hno,hna,jna,twt,tbar,fpo

  '''
#
#     need a fix for sched-odds  mismatch   ... almos fixed
#
  '''
OZ_ME_COLUMNS = { "nz_pk": " integer constraint nz_pk PRIMARY KEY autoincrement",         \
      "tdom":varchar,"nz_meet_no":num,"nz_venue":varchar,"nz_race":num,"nz_hno":num,    \
      "nz_hna":varchar,"nz_twt":real,"nz_tbar":num,"nz_jna":varchar,"nz_fpo":real       \
            }

  '''
  race_save = race_dict.copy()
#
  loren = len( oren )-1
  for lso in range(len(sren)):

    horse_dict = race_save.copy()
#
#   need a dict holding pattern   horse_dict["nz_"] = 
#
#
    horse_dict["nz_hno"] = hno = sren[ lso ]["number"]
    horse_dict["nz_hna"] = hna = sren[ lso ]["name"].upper().replace( "'", '^' )
    if sren[ lso ]["jockey"]:
      jna = sren[ lso ]["jockey"].upper().replace( "'", '^' )
      if sren[ lso ]["scratched"]:
        jna = sren[ lso ]["jockey"].lower()
    else:
       jna = "None"

    horse_dict["nz_twt"] = twt = sren[ lso ]["weight"]
    horse_dict["nz_tbar"] = tbar = sren[ lso ]["barrier"]
#
    if lso <= loren:
      horse_dict["nz_fpo"] = fwin = oren[ lso ]["ffwin"]
      fno = oren[ lso ]["number"]
      horse_dict["nz_jna"] = fjna = jna.replace( "'", '^' )
      fscr = oren[ lso ]["scr"]
    else:
      fwin,fnof,fjna,fscr = "98.76",hno,"SCRATCHED2>>> >",True
    print( hno,fno,hna,jna,twt,tbar,"++",fjna,fwin ,'============',fscr)
    lib_sq_write( horse_dict,curs,con,table )
#
#------------------------------------------------------------------
#
def OZ_races( mein,odin,roznz_dict,curs,con,table ):
  '''
         take each race info from meeting and odds
#
#              meetings.keys
#     betslip code,country,name,number,nz,pen,races[],status,track-dir,type,venue
#
#
#             race keys from oddsin
#    oddsin["meetings"][nra]["races"][0].keys()
#  'entries', 'number', 'plcpool', 'pools', 'qlapool', 'tfapool', 'winpool'
#
OZ_ME_COLUMNS = { "nz_pk": " integer constraint nz_pk PRIMARY KEY autoincrement",         \
      "tdom":varchar,"nz_meet_no":num,"nz_venue":varchar,"nz_race":num,"nz_hno":num,    \
      "nz_hna":varchar,"nz_twt":real,"nz_tbar":num,"nz_jna":varchar,"nz_fpo":real       \
            }

  '''

  roznz_save = roznz_dict.copy()

  len_o1 = len( odin["races"] ) -1
  for len_m in range(len(mein["races"])):
    '''
         load a fresh dictionary each race
    '''
    race_dict = roznz_save.copy()

    srain = mein["races"][len_m]
    sren = srain["entries"]
#
#     match qtab nztab :: race=1,horse=1,and barrier
#         if all 3 match then we have the same meeting
#
    race_dict["nz_venue"] = sraven = mein["venue"]

    srano = (99,99)
#
    race_dict["nz_race"] = srain["overseas_number"]

    srano  = ( srain["overseas_number"], srain["number"] )
#
    if len_m <= len_o1:
      orain = odin["races"][len_m]
      oren = orain["entries"]
    else:
      print(sraven)
      print( "LEN_M::",len_m,len(mein["races"]),"LEN_O1::::",len_o1,len(odin["races"]) )
      print("##+++ %s:::number of race mis match mein::: %3d  odin::: %3d" %( sraven,len(mein["races"]),len(odin["races"])))
      print("********************* this is not right ******************")
      continue
    print( "\n%s:" %( sraven ),srano )
    OZ_horses( sren,oren,race_dict,curs,con,table )
  return sraven
#
#------------------------------------------------------------------
#
def Ozm( schedin,oddsin,roznz_dict,curs,con,table ):
  '''
     input dict for nz tab schedule

OZ_ME_COLUMNS = { "nz_pk": " integer constraint nz_pk PRIMARY KEY autoincrement",         \
      "tdom":varchar,"nz_meet_no":num,"nz_venue":varchar,"nz_race":num,"nz_hno":num,    \
      "nz_hna":varchar,"nz_twt":real,"nz_tbar":num,"nz_jna":varchar,"nz_fpo":real       \
            }

  '''
#
  mee_mee = []

  roznz_dict["tdom"] = mee_date = schedin["date"]

  mee_au = only_aus( schedin["meetings"] )
  print(mee_au)
#
  for ea_oz_me in mee_au:
    mei = schedin["meetings"][ ea_oz_me ]
    oei = oddsin["meetings"][ ea_oz_me ]

    roznz_dict["nz_meet_no"] = mei["number"]

    print("\nnumber", mei["number"] ,oei["number"],"# ########### meeting code Mnn" )
    mein = schedin["meetings"][ ea_oz_me ]
    odin = oddsin["meetings"][ ea_oz_me ]
#
# here is the nz meeting # for the OZ meeting
#
    omeid = odin["number"]
    mee_mee.append( OZ_races( mein,odin,roznz_dict,curs,con,table ) )
  return mee_mee
  exit()
#
#------------------------------------------------------------------
#
def roznz():
  '''
/usr/local/share/public/git/roznz
         json nz tab has meeting  data
     it is split across sites DUHHHHH
     We have to read two of them to get our data
    1/ schedule  which gives races and horses
    2/ odds      which gives fixed odds 

most of the runtime is network latency

    so this takes twice as long to read

    it saves them minimal bandwidth if we only
    want odds. But that is stupid. Odds by
    themselves are meaningless

  '''
#  print("ROZ")
  roznz_table = "roznz"
  curs,con = lib_create_db(db_dir, db_db )

  SQX,roznz_dict = lib_sq_table( curs, roznz_table, OZ_ME_COLUMNS, OZ_ME_CONSTRAINT )
#  SQX is "create table ...."
#     roznz_dict is dict{ "column name": data type ( int,real,varchar )
#
  curs.execute( SQX )
  con.commit()
#
  url = "https://json.tab.co.nz/odds/"
  mee_odds = read_mee( url )
#
  url = "https://json.tab.co.nz/schedule/"
  mee_sched = read_mee( url )
#
#  print(url)
  return Ozm( mee_sched,mee_odds,roznz_dict,curs,con,roznz_table )
#
#------------------------------------------------------------------
#
if __name__ == "__main__":
  '''
  do nz json
       odds, schedule
  '''
  debug = roznz()
  print( debug )
exit()
#########################################################################

